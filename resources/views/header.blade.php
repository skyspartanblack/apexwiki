<header>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-danger ">
        <div class="container">
          <a class="navbar-brand" href="/">
            <img src="Recursos/apex logo black.png" alt="" width="30" height="24">
          </a>
            <a class="navbar-brand fw-bolder" href="/">Apex Legends Wiki</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>      
        <div class="collapse navbar-collapse" id="navbar">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="#main">Conoce Apex<span class="sr-only"></span></a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="#leyendas">Leyendas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#armas">Armas</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#ranks">Clasificatorias</a>
              </li>
          </ul>
        </div>
      </nav>
    </div>    
</header>